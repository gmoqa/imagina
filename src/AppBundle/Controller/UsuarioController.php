<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Usuario;
use AppBundle\Form\UsuarioType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Usuario controller.
 *
 * @Route("/usuario")
 */
class UsuarioController extends Controller
{

    /**
     * Return a index template
     *
     * @Route("/", name="usuario")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        return array('template' => "empty");
    }

    /**
     * Lists all Usuario entities.
     *
     * @Route("/results", name="usuario_results")
     * @Method("GET")
     * @Template()
     */
    public function resultsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AppBundle:Usuario')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Displays a form to create a new Unidad entity.
     *
     * @Route("/new", name="usuario_new")
     * @param Request $request
     * @Method({"GET","POST"})
     * @Template()
     * @return array|Response
     */
    public function newAction(Request $request)
    {
        $entity = new Usuario();
        $form   = $this->createEntityForm($entity);
        $roles  = $this->container->getParameter('app.roles');

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $userManager = $this->container->get('fos_user.user_manager');
                $entity->setEnabled(1);

                foreach ($form['rolesAuxiliar']->getData() as $role)
                {
                    $entity->addRole($role);
                }
                $userManager->updateUser($entity);
                return new Response('success');
            }
        }

        return array(
            'entity' => $entity,
            'roles'  => $roles,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Usuario entity.
     *
     * @Route("/{id}", name="usuario_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Usuario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuario entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Usuario entity.
     *
     * @Route("/{id}/edit", name="usuario_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Usuario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity.');
        }

        $form =  $this->createEntityForm($entity);

        $form->handleRequest($request);
        if ($request->getMethod() === 'POST') {
            if ($form->isValid()) {
                $userManager = $this->container->get('fos_user.user_manager');

                $roles = $entity->getRoles();

                foreach ($roles as $role) {
                    $entity->removeRole($role);
                }

                foreach ($form['rolesAuxiliar']->getData() as $role) {
                    $entity->addRole($role);
                }
                $userManager->updateUser($entity);
                return new Response('success');
            }
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
    * Creates a form to edit a Usuario entity.
    *
    * @param Usuario $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Usuario $entity)
    {
        $form = $this->createForm(new UsuarioType(), $entity, array(
            'action' => $this->generateUrl('usuario_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Usuario entity.
     *
     * @Route("/{id}", name="usuario_update")
     * @Method("PUT")
     * @Template("AppBundle:Usuario:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Usuario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuario entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('usuario_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Usuario entity.
     *
     * @Route("/{id}", name="usuario_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Usuario')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Usuario entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('usuario'));
    }

    /**
     * Creates a form to delete a Usuario entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('usuario_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Creates a form to create a Autoridad entity.
     *
     * @param Usuario $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEntityForm(Usuario $entity)
    {
        return $this->createForm(new UsuarioType(), $entity,
            array(
                'app_roles' => $this->container->getParameter('app.roles')
            )
        );
    }


    /**
     * @Route("/supervisor/comparar", name="supervisor_comparar_index")
     */
    public function comparaUsuariosIndex()
    {
        $em = $this->getDoctrine()->getManager();
        $usuarios = $em->getRepository('AppBundle:Usuario')->getUsuariosValidos();
        return $this->render(
            'AppBundle:Usuario:comparacionSupervisores.html.twig',
            [
                'usuarios' => $usuarios
            ]
        );
    }


    /**
     * @param Request $request
     * @method ("POST")
     * @Route("supervisor/comparacion", name="reportes_supervisor_comparacion")
     */
    public function getChartsComparationResult(Request $request)
    {
        $usuarios = $request->request->get('usuarios');

        if(is_array($usuarios)) {
            $em = $this->getDoctrine();
            $rs = $em->getRepository('AppBundle:Usuario')->getComparacionUsuarios($usuarios);
            echo $this->renderView(
                "AppBundle:Usuario:columnaDetalleUsuario.html.twig",
                array(
                    "values" => $rs
                )
            );
        }else{
            echo "Seleccione Al Menos Una Obra";
        }

        die;
    }

    /**
     * @param Request $request
     * @param $id
     * @return Response
     * @Route("actualizar/avatar/{id}", name="actualizar_avatar_usuario")
     */
    public function updateAvatar(Request $request, $id)
    {
        if ($request->getMethod() === 'POST') {
            $userManager = $this->container->get('fos_user.user_manager');
            $usuario = $userManager->findUserBy(['id'=>$id]);
            $file = $this->getRequest()->files->get('file');
            if ($file) {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                $pathDocument = $this->container
                        ->getParameter('kernel.root_dir') . '/../web/uploads/avatars';
                $file->move(
                    $pathDocument,
                    $fileName
                );
                $usuario->setAvatar($fileName);
                $userManager->updateUser($usuario);
                return new Response('success');
            }

        } else {
            return $this->render('AppBundle:Usuario:updateAvatar.html.twig');
        }
    }
}


