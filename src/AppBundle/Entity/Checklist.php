<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Checklist
 *
 * @author Guillermo Quinteros <gu.quinteros@gmail.com> 2015
 *
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="Checklist")
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ChecklistRepository")
 */
class Checklist
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var Estado
     *
     * @ORM\ManyToOne(targetEntity="Estado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="estado", referencedColumnName="id")
     * })
     */
    protected $estado;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", name="finalizado", nullable=true)
     */
    protected $finalizado;

    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario", referencedColumnName="id")
     * })
     */
    protected $usuario;

    /**
     * @var Tarea
     *
     * @ORM\ManyToOne(targetEntity="Tarea")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tarea", referencedColumnName="id")
     * })
     */
    protected $tarea;

    /**
     * @var Obra
     *
     * @ORM\ManyToOne(targetEntity="Obra")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="obra", referencedColumnName="id")
     * })
     */
    protected $obra;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="fecha_comprometida", type="datetime", nullable=true)
     */
    protected $fechaComprometida;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="fecha_completado", type="datetime", nullable=true)
     */
    protected $fechaCompletado;

    /**
     * @var string
     * @ORM\Column(name="observacion", type="text", nullable=true)
     */
    private $observacion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="tarea_atrasada", type="boolean", nullable=true)
     */
    private $tareaAtrasada;

    /**
     * @var string
     *
     * @ORM\Column(name="file", type="string", length=255, nullable=true)
     */
    private $file;

    /**
     * @var boolean
     *
     * @ORM\Column(name="omitir", type="boolean", nullable=true)
     */
    private $omitir;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Estado
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param $estado
     * @return $this
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param $fecha
     * @return $this
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isFinalizado()
    {
        return $this->finalizado;
    }

    /**
     * @param $finalizado
     * @return $this
     */
    public function setFinalizado($finalizado)
    {
        $this->finalizado = $finalizado;

        return $this;
    }

    /**
     * @return Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param $usuario
     * @return $this
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * @return Tarea
     */
    public function getTarea()
    {
        return $this->tarea;
    }

    /**
     * @param $tarea
     * @return $this
     */
    public function setTarea($tarea)
    {
        $this->tarea = $tarea;

        return $this;
    }

    /**
     * @return Obra
     */
    public function getObra()
    {
        return $this->obra;
    }

    /**
     * @param $obra
     * @return $this
     */
    public function setObra($obra)
    {
        $this->obra = $obra;

        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getFechaComprometida()
    {
        return $this->fechaComprometida;
    }

    /**
     * @param $fechaComprometida
     * @return $this
     */
    public function setFechaComprometida($fechaComprometida)
    {
        $this->fechaComprometida = $fechaComprometida;

        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getFechaCompletado()
    {
        return $this->fechaCompletado;
    }

    /**
     * @param $fechaCompletado
     * @return $this
     */
    public function setFechaCompletado($fechaCompletado)
    {
        $this->fechaCompletado = $fechaCompletado;

        return $this;
    }

    /**
     * @return string
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * @param $observacion
     * @return $this
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isTareaAtrasada()
    {
        return $this->tareaAtrasada;
    }

    /**
     * @param $tareaAtrasada
     * @return $this
     */
    public function setTareaAtrasada($tareaAtrasada)
    {
        $this->tareaAtrasada = $tareaAtrasada;

        return $this;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param $file
     * @return $this
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isOmitir()
    {
        return $this->omitir;
    }

    /**
     * @param $omitir
     * @return $this
     */
    public function setOmitir($omitir)
    {
        $this->omitir = $omitir;

        return $this;
    }
}
