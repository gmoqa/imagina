<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class TareaController
 * @package AppBundle\Controller
 * @Route("/tarea")
 * @author Guillermo Quinteros <gu.quinteros@gmail.com> 2015
 */
class TareaController extends CrudController
{
    const ENTITY_NAME = "Tarea";

    const ENTITY_NAMESPACE = "AppBundle\\Entity\\Tarea";

    const TYPE_NAMESPACE = "AppBundle\\Form\\TareaType";

    const ADD_FILE = TRUE;

    /**
     * Get List of tareas atrasadas.
     *
     * @Route("/atrasadas", name="list_tareas_atrasadas")
     * @Method({"GET"})
     * @Template()
     */
    public function listTareasAtrasadasAction()
    {
        $em       = $this->getDoctrine();
        $entities = $em->getRepository('AppBundle:Checklist')->getChecklistAtrasados();

        return array(
            'checklists' => $entities,
        );
    }
}
