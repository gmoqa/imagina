<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\Usuario;

/**
 * Class UsuarioLoadData
 * @package AppBundle\DataFixtures\ORM
 * @author Guillermo Quinteros <gu.quinteros@gmail.com> 2015
 */
class UsuarioLoadData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $userManager = $this->container->get('fos_user.user_manager');

        /** @var Usuario $usuario1 */
        $usuario1 = $userManager->createUser();

        $usuario1->setUsername('testuser');
        $usuario1->setNombres('Admin');
        $usuario1->setApellidos('User');
        $usuario1->setEmail('testuser@imagina.cl');
        $usuario1->setPlainPassword('123456');
        $usuario1->setEnabled(true);
        $usuario1->setRoles(array('ROLE_ADMIN'));
        $userManager->updateUser($usuario1, true);

        $this->addReference('usuario1', $usuario1);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 0;
    }
}
