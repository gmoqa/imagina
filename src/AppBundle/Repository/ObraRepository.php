<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class ObraRepository
 *
 * @package AppBundle\EntityRepository
 *
 * @author  Guillermo Quinteros <gu.quinteros@gmail.com> 2016
 */
class ObraRepository extends EntityRepository
{
    public function getObrasDashboard($ids)
    {
        $em = $this->getEntityManager();
        $arrAux = [];
        foreach ($ids as $id) {
            $dql = "SELECT
			obra.id,
			obra.nombre,
			responsable.nombres,
			responsable.apellidos
			FROM AppBundle:Obra obra
			JOIN obra.responsable responsable
			WHERE 1 = 1
			";
            $dql.="and obra.id=".$id;

            $query = $em->createQuery($dql);
            $results = $query->getResult();

            foreach ($results as $value) {

                $checklist=$this->getEsperado("", $value['id']);
                $checklistCompletados = $em->getRepository('AppBundle:Checklist')
                    ->findBy(
                        [
                            'obra' => $value['id'],
                            'finalizado' => true
                        ]
                    );
                if (count($checklist) > 1) {
                    $value["total"]=count($checklist);
                    $value["completados"]=count($checklistCompletados);
                    $porcentaje = count($checklistCompletados) * 100 / $value["total"];
                    $value['porcentaje'] = round($porcentaje);
                } else {
                    $value["total"]=0;
                    $value["completados"]=0;
                    $value['porcentaje']=0;
                }

                $arrAux[] = $value;
            }
        }
        return $arrAux;
    }

    public function getObrasDashboard2()
    {
        $em = $this->getEntityManager();

        $dql = "SELECT
			obra.id,
			obra.nombre,
			responsable.nombres,
			responsable.apellidos
			FROM AppBundle:Obra obra
			JOIN obra.responsable responsable
			WHERE 1 = 1
			";

        $query = $em->createQuery($dql);
        $results = $query->getResult();

        $arrAux = [];

        foreach ($results as $value) {
            $checklist = $em->getRepository('AppBundle:Checklist')->findBy(['obra' => $value['id']]);
            
            $checklistCompletados = $em->getRepository('AppBundle:Checklist')
                ->findBy(
                    [
                        'obra' => $value['id'],
                        'finalizado' => true
                    ]
                );
            
            $lastChecklist = $em->getRepository('AppBundle:Checklist')->findBy(['obra' => $value['id']]);

            if (count($checklist) > 1) {
                $porcentaje = count($checklistCompletados) * 100 / count($checklist);
                $value['porcentaje'] = round($porcentaje);
            } else {
                $value['porcentaje'] = 0;

            }

            $arrAux[] = $value;
        }

        return $arrAux;

    }

    /**
     * @param $idObra
     * @return array
     */
    public function getRendimientoCumplimiento($idObra)
    {
        $months = [];

        for ($i = 0; $i < 12; $i++) {
            $months[] = date("Y-m".'-01', strtotime( date( 'Y-m-01' )." -$i months"));
        }

        $arrResult = [];

        foreach (array_reverse($months) as $month) {
            $array = [];
            $array['esperado'] = count($this->getEsperado($month, $idObra));
            $array['real'] = count($this->getEsperado($month, $idObra, true));
            $array['porcentaje'] = $array['real']?($array['real']/$array['esperado'])*100:0;
            $arrResult[$month] = $array;
        }

        return $arrResult;
    }

    public function getRendimientoCumplimientoCompleto($idObra)
    {
        $em = $this->getEntityManager();
        $obra = $em->getRepository("AppBundle:Obra")->find($idObra);
        $primeraFecha = $this->getPrimerChecklist($idObra);
        $months = [];
        $i = 0;
        //modificacion para poner como fecha maxima, la fecha actual, si la fecha estimada de termino es mayor a la fecha actual.
        $time = new \DateTime();
        $anoActual = $time->format("Y");

        $strDate = $time->format("Y-m");

//        if(intval($obra->getFechaTermino()->format('Y')) > intval($anoActual)){
//            $strDate = $time->format("Y-m");
//        }else{
//            $strDate = $obra->getFechaTermino()->format('Y-m');
//        }

        if($primeraFecha != null){
            do {
                $months[] = date("Y-m" . '-01', strtotime($strDate . '-01' . " -$i months"));
                ++$i;
            }while($primeraFecha != $months[$i-1]);
        }else{
            $months=array();
        }

        $arrResult = [];
        if(sizeof($months)>1) {
            foreach (array_reverse($months) as $month) {
                $array = [];
                $array['esperado'] = count($this->getEsperado($month, $idObra));
                $array['real'] = count($this->getEsperado($month, $idObra, true));
                $array['porcentaje'] = $array['real'] ? ($array['real'] / $array['esperado']) * 100 : 0;
                $arrResult[$month] = $array;
            }
//            echo '<pre>';
//            var_dump($arrResult);
            return $arrResult;
        }else{
            return null;
        }
    }

    public function getRendimientoCumplimientoCompletoRendimiento($idObra)
    {
        $em=$this->getEntityManager();
        $obra=$em->getRepository("AppBundle:Obra")->find($idObra);

        $primeraFecha=$this->getPrimerChecklist($idObra);
        $months = [];
        $i=0;
        //modificacion para poner como fecha maxima, la fecha actual, si la fecha estimada de termino es mayor a la fecha actual.
        $time = new \DateTime();
        $anoActual=$time->format("Y");
        $strDate=$time->format("Y-m");
//        if(intval($obra->getFechaTermino()->format('Y'))>intval($anoActual)){
//            $strDate=$time->format("Y-m");
//        }else{
//            $strDate=$obra->getFechaTermino()->format('Y-m');
//        }

        if($primeraFecha!=null){
            do {
                $months[] = date("Y-m" . '-01', strtotime($strDate . '-01' . " -$i months"));
                ++$i;
            }while($primeraFecha != $months[$i-1]);
        }else{
            $months=array();
        }


        $arrResult = [];
        if(sizeof($months)>1) {
            foreach (array_reverse($months) as $month) {
                $array = [];
                $array['esperado'] = count($this->getEsperado($month, $idObra));
                $array['real'] = count($this->getEsperadoRendimiento($month, $idObra, true));
                $array['porcentaje'] = $array['real'] ? ($array['real'] / $array['esperado']) * 100 : 0;
                $arrResult[$month] = $array;
            }
//            echo '<pre>';
//            var_dump($arrResult);
            return $arrResult;
        }else{
            return null;
        }
    }


    /**
     * @param $date
     * @param $idObra
     * @param null $finalizado
     * @return mixed
     */
    private function getEsperado($date, $idObra, $finalizado = null)
    {
        $em = $this->getEntityManager();
        $dql = "SELECT
			checklist.id
			FROM AppBundle:Checklist checklist
			JOIN checklist.obra obra
			WHERE 1 = 1
			AND obra.id = :obra
			AND checklist.fechaComprometida <= :fecha
			";

        if ($finalizado) {
            $dql = $dql." AND checklist.finalizado = TRUE";
        }

        $query = $em->createQuery($dql);

        if($date != "") {
            $query->setParameter('fecha', new \DateTime($date));
        }else {
            $query->setParameter('fecha', new \DateTime());
        }

        $query->setParameter('obra', $idObra);
        $results = $query->getResult();

        return $results;

    }

    /**
     * @param $date
     * @param $idObra
     * @param null $finalizado
     * @return mixed
     */
    private function getEsperadoRendimiento($date, $idObra, $finalizado = null)
    {
        $em = $this->getEntityManager();
        $dql = "SELECT
			checklist.id
			FROM AppBundle:Checklist checklist
			JOIN checklist.obra obra
			WHERE 1 = 1
			AND obra.id = :obra
			AND checklist.fechaComprometida <= :fecha
			";

        if ($finalizado) {
            $dql = $dql." AND checklist.finalizado = TRUE";
            $dql = $dql." AND checklist.tareaAtrasada IS NULL";
        }

        $query = $em->createQuery($dql);

        if($date != "") {
            $query->setParameter('fecha', new \DateTime($date));
        }else {
            $query->setParameter('fecha', new \DateTime());
        }

        $query->setParameter('obra', $idObra);
        $results = $query->getResult();

        return $results;

    }

    public function getAcumulado()
    {
        $months = [];
        for ($i = 0; $i < 12; $i++) {
            $months[] = date("Y-m".'-01', strtotime( date( 'Y-m-01' )." -$i months"));
        }

        $arrResult = [];

        foreach (array_reverse($months) as $month) {
            $array = [];
            $array['esperado'] = count($this->getEsperadoAcumulado($month));
            $array['real'] = count($this->getEsperadoAcumulado($month, true));
            $porcentaje = $array['real']?($array['real']/$array['esperado'])*100:0;
            $array['porcentaje'] = round($porcentaje);
            $arrResult[$month] = $array;
        }

        return $arrResult;
    }

    /**
     * @param $date
     * @param null $finalizado
     * @return mixed
     */
    private function getEsperadoAcumulado($date, $finalizado = null)
    {
        $em = $this->getEntityManager();
        $dql = "SELECT
			checklist.id
			FROM AppBundle:Checklist checklist
			WHERE 1 = 1
			AND checklist.fechaComprometida <= :fecha
			";

        if ($finalizado) {
            $dql = $dql." AND checklist.finalizado IS NOT NULL";
        }

        $query = $em->createQuery($dql);
        $query->setParameter('fecha', new \DateTime($date));
        $results = $query->getResult();

        return $results;

    }

    private function getPrimerChecklist($idObra){
        $em = $this->getEntityManager();
        $chk=$em->getRepository('AppBundle:Checklist')
            ->findOneBy(
                [
                    'obra' => $idObra
                ]
                ,[
                    'fechaComprometida' => 'ASC'
                ]
            );
        if($chk)
            return($chk->getFechaComprometida()->format('Y-m').'-01');
        else
            return null;
    }
}