<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Estado;

/**
 * Class EstadoLoadData
 * @package AppBundle\DataFixtures\ORM
 * @author Guillermo Quinteros <gu.quinteros@gmail.com> 2015
 */
class EstadoLoadData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $entity1 = new Estado();
        $entity1->setNombre('Pendiente');

        $entity2 = new Estado();
        $entity2->setNombre('Completado');

        $entity3 = new Estado();
        $entity3->setNombre('Omitido');

        $manager->persist($entity1);
        $manager->persist($entity2);
        $manager->persist($entity3);

        $manager->flush();

        $this->addReference('EstadoPendiente', $entity1);
        $this->addReference('EstadoCompleto', $entity2);
        $this->addReference('EstadoOmitido', $entity3);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
}
