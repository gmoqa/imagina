<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class ChecklistRepository
 *
 * @package AppBundle\EntityRepository
 *
 * @author  Guillermo Quinteros <gu.quinteros@gmail.com> 2016
 */
class ChecklistRepository extends EntityRepository
{
    public function getChecklistByObra($idObra)
    {
        $em = $this->getEntityManager();
        $dql = "SELECT
			checklist.id,
			tarea.nombre,
			tarea.attachmentRequired,
			tarea.emailNotification,
			checklist.fechaComprometida,
			checklist.finalizado,
			checklist.file as checklistFile,
			tarea.file as tareaFile,
			checklist.tareaAtrasada,
		    obra.nombre as nombreObra
			FROM AppBundle:Checklist checklist
			JOIN checklist.obra obra
			JOIN checklist.tarea tarea
			WHERE 1 = 1
			AND obra.id = :obra
			ORDER BY checklist.fechaComprometida ASC
			";

        $query = $em->createQuery($dql);
        $query->setParameter('obra', $idObra);
        $results = $query->getResult();

        $arrAux = [];

        $today = new \DateTime();

        foreach ($results as $value) {
            $fecha = new \DateTime($value['fechaComprometida']->format('d-m-Y'));
            $restante = $fecha->diff($today);
            $date=str_replace("-", "", $restante->format('%R%a'));
            $date=str_replace("+", "", $date);

            $value['restantes'] = $date;
            $arrAux[] = $value;
        }

        return $arrAux;

    }

    /**
     * @return array
     */
    public function getChecklistAtrasados()
    {
        $em = $this->getEntityManager();
        $dql = "SELECT
			checklist
			FROM AppBundle:Checklist checklist
			WHERE 1 = 1
			AND checklist.fechaComprometida < :today
			AND checklist.finalizado is null
			";

        $query = $em->createQuery($dql);
        $query->setParameter('today', new \DateTime());
        $results = $query->getResult();

        return $results;
    }

    /**
     * @param $idObra
     * @return array
     */
    public function getChecklistWithFile($idObra)
    {
        $em = $this->getEntityManager();
        $dql = "SELECT
			checklist
			FROM AppBundle:Checklist checklist
			WHERE 1 = 1
			AND checklist.file is not NULL 
			AND checklist.obra = :obra
			";

        $query = $em->createQuery($dql);
        $query->setParameter('obra', $idObra);
        $results = $query->getResult();

        return $results;
    }
}


