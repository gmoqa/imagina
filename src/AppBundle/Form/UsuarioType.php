<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsuarioType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email',
                array(
                    'attr'  =>
                        array(
                            'placeholder'=>'Email'
                        )
                )
            )
            ->add('nombres', 'text',
                array(
                    'attr'  =>
                        array(
                            'placeholder'=>'Nombres'
                        )
                )
            )
            ->add('apellidos', 'text',
                array(
                    'attr'  =>
                        array(
                            'placeholder'=>'Apellidos'
                        )
                )
            )
            ->add('plainPassword', 'repeated',
                array(
                    'type' => 'password',
                    'first_options' =>
                        array(
                            'label' => 'Contraseña',
                            'attr'  =>
                                array(
                                    'placeholder'=>'Contraseña'
                                )
                        ),
                    'second_options' =>
                        array(
                            'label' => 'Repita contraseña',
                            'attr'  =>
                                array(
                                    'placeholder'=>'Verificar Contraseña'
                                )
                        ),
                    'invalid_message' => 'Las contraseñas deben ser las mismas',
                )
            )
            ->add('username', 'text',
                array(
                    'attr'  =>
                        array(
                            'placeholder'=>'Nombre Usuario'
                        )
                )
            )
            ->add('rolesAuxiliar', 'choice',
                array(
                    'choices'     => $options['app_roles'],
                    'mapped'      => false,
                    'multiple'    => true,
                    'empty_value' => 'Seleccionar Perfíl',
                    'label'       => 'Rol/es',
                    'attr'        =>
                        array(
                            'placeholder'=>'Seleccionar Perfiles'
                        ),
                )
            )

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Usuario',
            'app_roles' => array()
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_usuario';
    }
}
