<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
/**
 * Class SendEmailCommand
 *
 * @package AppBundle\Command
 *
 * author Guillermo Quinteros <gu.quinteros@gmail.com>
 */
class SendEmailsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('send:emails')
            ->setDescription('Send a emails...')
        ;
    }

    /**
     * Que elegancia la de Francia
     * From Chile with ♥
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return Object
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Concepto Pacific - Fotos notariales')
            ->setFrom('gu.quinteros@gmail.com')
            ->setTo('gu.quinteros@gmail.com')
            ->setBody(
                $this
                    ->getContainer()
                    ->get('templating')
                    ->render('AppBundle:Templates:TareaAtrasada.html.twig')
                ,
                'text/html'
            )
        ;

        $mailer = $this->getContainer()->get('mailer');

        if ($mailer->send($message)) {
            $output->writeln('<info> OK...</info>');
        } else {
            $output->writeln('<info> ERROR...</info>');
        }
    }


    /**
     * @param $body
     * @param $to
     * @return bool
     */
    public function sendEmail($body, $to)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject($this->getContainer()->getParameter('email.gmail.subject'))
            ->setFrom($this->getContainer()->getParameter('email.gmail.account'))
            ->setTo($to)
            ->setBody($body)
        ;

        $this
            ->getContainer()
            ->get('mailer')
            ->send($message);

        return true;
    }
}
