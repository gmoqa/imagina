<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class CrudController created with ❤ in Chile
 * @package AppBundle\Controller
 * @author Guillermo Quinteros <gu.quinteros@gmail.com> 2015
 *
 */
abstract class CrudController extends Controller
{
    const ENTITY_NAME = null;

    const ENTITY_NAMESPACE = null;

    const TYPE_NAMESPACE = null;

    const ADD_FILE = null;

    /**
     * Return a index
     *
     * @Route("/")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        return array(
            'template' => 'empty',
        );
    }

    /**
     * Lists all entities.
     *
     * @Route("/results")
     * @Method("GET")
     * @Template()
     */
    public function resultsAction()
    {
        $entities = $this->getRepository()->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Displays a form to create a new entity.
     *
     * @Route("/new")
     * @param Request $request
     * @Method({"GET","POST"})
     * @Template()
     * @return array|Response
     */
    public function newAction(Request $request)
    {
        $entityName = $this::ENTITY_NAMESPACE;
        $entity = new $entityName;
        $form =  $this->getEntityForm($entity);

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                if ($this::ADD_FILE) {
                    /** @var File $file */
                    $file = $entity->getFile();
                    $fileName = md5(uniqid()).'.'.$file->getExtension();
                    $pathDocument = $this->container
                            ->getParameter('kernel.root_dir') . '/../web/uploads/documentos';
                    $file->move(
                        $pathDocument,
                        $fileName
                    );
                    $entity->setFile($fileName);
                }
                $em->persist($entity);
                $em->flush();
                return new Response('success');
            }
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a entity.
     *
     * @Route("/{id}")
     * @Method("GET")
     * @Template()
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function showAction($id)
    {
        $entity = $this->getRepository()->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity.');
        }

        return array(
            'entity' => $entity
        );
    }

    /**
     * Displays a form to edit an existing entity.
     *
     * @Route("/{id}/edit")
     * @Method({"GET","POST"})
     * @Template()
     * @param $id
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(Request $request, $id)
    {
        $entity = $this->getRepository()->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity.');
        }

        $form =  $this->getEntityForm($entity);
        $form->handleRequest($request);
        if ($request->getMethod() === 'POST') {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                if ($this::ADD_FILE) {
                    /** @var File $file */
                    $file = $entity->getFile();

                    if ($file) {
                        $fileName = md5(uniqid()).'.'.$file->guessExtension();
                        $pathDocument = $this->container
                                ->getParameter('kernel.root_dir') . '/../web/uploads/documentos';
                        $file->move(
                            $pathDocument,
                            $fileName
                        );
                        $entity->setFile($fileName);
                    }
                }
                $em->persist($entity);
                $em->flush();

                return new Response('success');
            }

//            foreach ($form->getErrors() as $error){
//                var_dump($error->getMessage());
//            }
//            die;
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Delete a entity.
     *
     * @Route("/{id}")
     * @Method("POST")
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id)
    {
        $entity = $this->getRepository()->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity.');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();

        return new Response('success');
    }

    /**
     * @return mixed
     */
    private function getRepository()
    {
        $em = $this->getDoctrine()->getManager();
        return $repository = $em->getRepository('AppBundle:'.$this::ENTITY_NAME);
    }

    /**
     * @param $entity
     * @return \Symfony\Component\Form\Form
     */
    private function getEntityForm($entity)
    {
        $entityType = $this::TYPE_NAMESPACE;
        return  $this->createForm(new $entityType, $entity);
    }
}
