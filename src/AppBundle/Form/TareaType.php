<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;


class TareaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'nombre',
                'text',
                array(
                    'attr'  =>
                        array(
                            'placeholder'=>'Nombre'
                        )
                )
            )
            ->add('attachment_required', CheckboxType::class, array(
                'label'    => '¿Adjunto Requerido?',
                'required' => false,
            ))

            ->add('email_notification', CheckboxType::class, array(
                'label'    => '¿Notificación por Email?',
                'required' => false,
            ))
            ->add('file', FileType::class, array('label' => 'Suba un Archivo.', 'data_class' => null))
//            ->add(
//                'duracion',
//                'integer',
//                array(
//                    'attr'  =>
//                        array(
//                            'placeholder'=>'Duración aprox..'
//                        )
//                )
//            )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Tarea'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_tarea';
    }
}
