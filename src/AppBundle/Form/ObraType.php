<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ObraType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'nombre',
                'text',
                array(
                    'attr'  =>
                        array(
                            'placeholder'=>'Nombre'
                        )
                )
            )
            ->add('direccion', 'text',
                array(
                    'attr'  =>
                        array(
                            'placeholder'=>'Dirección'
                        )
                )
            )
            ->add(
                'responsable',
                'entity',
                array(
                    'class'         => 'AppBundle:Usuario',
                    'property'      => 'fullName',
                    'empty_value'   => 'Seleccionar Responsable',
                    'query_builder' => function (EntityRepository $repository) {
                        return $repository->createQueryBuilder('u')->orderBy('u.apellidos', 'ASC');
                    }
                , 'attr'        =>
                    array(
                        'placeholder' => 'Seleccionar Responsable',
                        'data-style'  => 'btn-white'
                    )
                )
            )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Obra'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_obra';
    }
}
