paths        = {};
entityName       = "";
hasDatatable     = false;
sUrlDatatable    = false;
isFirstLoadTable = true;

function initializeCRUD(options) {
	paths = {
		_reload: options._reload,
		_show:   options._show,
		_new:    options._new,
		_edit:   options._edit,
		_delete: options._delete,
	}
	entityName    = options.entityName;
	hasDatatable  = options.hasDatatable;
	sUrlDatatable = options.sUrlDatatable;
	loadTable();
}

function loading(status){
	if (status) {
		$("#loading").fadeIn("slow", function() {
			$(this).removeClass("hidden");
		});
	}else{
		$("#loading").fadeOut("slow", function() {
			$(this).addClass("hidden");
		});
	}
}

function loadTable(){
	loading(true);
	$.ajax({
		type: 'GET',
		async: false,
		url: paths._reload,
		success: function(results) {
			$('#results').html('').html(results);
			loading(false);
			documentOnReady();
		},
		error: function(e){
			console.log(e);
		}
	});
}


function viewElement(id) {
	var path = paths._show;
	path = path.replace("idElemento", id);
	$.ajax({
		type: 'GET',
		async: true,
		url: path,
		success: function(results) {
			bootbox.dialog({
				message: results,
				buttons: {
					danger: {
						label: "<i class='fa fa-trash'></i> Eliminar",
						className: "btn-danger pull-left",
						callback: function() {
							deleteElement(id);
						}
					},
					warning: {
						label: "<i class='fa fa-pencil-square-o'></i> Editar",
						className: "btn-warning",
						callback: function() {
							editElement(id);
						}
					},
					success: {
						label: "<i class='fa fa-check'></i> Aceptar",
						className: "btn-success",
						callback: function() {
						}
					}
				}
			});
			// visual adaptations
			$('.bootbox-close-button').hide();
			$('.modal-body').css('padding',0);
			$('.panel').css('margin-bottom',0);
		},
		error: function(e){
			console.log(e);
		}
	});
}

function viewElementWithDisable(id, enabled) {
	var path = paths._show;
	path = path.replace("idElemento", id);
	$.ajax({
		type: 'GET',
		async: true,
		url: path,
		success: function(results) {
			var buttonsBootbox = {
				warning: {
					label: "Editar",
					className: "btn-warning",
					callback: function() {
						editElement(id);
					}
				},
				success: {
					label: "Aceptar",
					className: "btn-success",
					callback: function() {
					}
				}
			};

			if (enabled === 1) {
				var btnState = {
					danger: {
						label: 'Desabilitar',
						className: 'btn-danger pull-left',
						callback: function() {
							changeStateElement(id);
						}
					}
				};
			} else {
				var btnState = {
					success2: {
						label: 'Habilitar',
						className: 'btn-success pull-left',
						callback: function() {
							changeStateElement(id);
						}
					}
				};
			}

			$.extend(buttonsBootbox, btnState);

			bootbox.dialog({
				message: results,
				buttons: buttonsBootbox
			});
			// visual adaptations
			$('.bootbox-close-button').hide();
			$('.modal-body').css('padding',0);
			$('.panel').css('margin-bottom',0);
		},
		error: function(e){
			console.log(e);
		}
	});
}

function generateBootboxForm(options) {
	if ( !(options.hasOwnProperty('title')) ) {
		title = "Alert! Debes agregar el title";
	} else {
		title = options.title;
	}

	if ( !(options.hasOwnProperty('message')) ) {
		message = "Alert! Debes agregar el message";
	} else {
		message = options.message;
	}

	if ( !(options.hasOwnProperty('form')) ) {
		form = "Alert! Debes agregar el form";
	} else {
		form = options.form;
	}

	if ( !(options.hasOwnProperty('messageSuccess')) ) {
		messageSuccess = "Alert! Debes agregar el messageSuccess";
	} else {
		messageSuccess = options.messageSuccess;
	}

	bootbox.dialog({
		message: message,
		title: title,
		buttons: {
			success: {
				label: "<i class='fa fa-reply'></i> Volver",
				className: "btn-primary",
				callback: function() {
				}
			},
			main: {
				label: "<i class='fa fa-floppy-o'></i> Guardar",
				className: "btn-success",
				callback: function() {
					var formData = $(form).serializeArray();
					var path     = $(form).prop("action");
					$.ajax({
						type: "POST",
						async: true,
						data: formData,
						url: path,
						success: function(results) {
							if (results == "success") {
								bootbox.alert(messageSuccess, function() {
									loadTable();
								});
							}else{
								var options = {
									title:           title
									,message:        results
									,form:           form
									,messageSuccess: messageSuccess
								};
								generateBootboxForm(options);
							}
						},
						error: function(e){
							console.log(e);
						}
					});
				}
			}
		}
	});
	$('.bootbox-close-button').hide();
	$('.modal-body').css('padding',0);
	$('.panel').css('margin-bottom',0);
}

function addElement() {
	var path = paths._new;
	$.ajax({
		type: 'GET',
		async: true,
		url: path,
		success: function(results) {
			var options = {
				title:          "Añadir " + entityName
				,message:        results
				,form:           "#the-form"
				,messageSuccess: "Agregado Correctamente"
			};
			generateBootboxForm(options);
		},
		error: function(e){
			console.log(e);
		}
	});
};

function editElement(id) {
	var path = paths._edit;
	path = path.replace("idElemento", id);
	$.ajax({
		type: 'GET',
		async: true,
		url: path,
		success: function(results) {
			var options = {
				title:          "Editar " + entityName
				,message:        results
				,form:           "#the-form"
				,messageSuccess: "Editado Correctamente"
			};
			generateBootboxForm(options);
		},
		error: function(e){
			console.log(e);
		}
	});
}

function deleteElement(id) {
    bootbox.hideAll();
	div = "<div class='alert alert-danger alert-dark'>"+
			"<strong>Alerta!</strong> Desea realmente eliminar el/la " + entityName + ". <br>"+
			"Esto eliminará todos los elementos asociados."+
			"</div>";
	bootbox.dialog({
		message: div,
		buttons: {
			info: {
				label: "<i class='fa fa-reply'></i> Volver",
				className: "btn-primary",
				callback: function() {
				}
			},
			danger: {
				label: "<i class='fa fa-trash'></i> Eliminar",
				className: "btn-danger",
				callback: function() {
					var path = paths._delete;
					path = path.replace("idElemento", id);
					$.ajax({
						type: "POST",
						async: true,
						url: path,
						success: function(results) {
							bootbox.alert(entityName + " Eliminada/o correctamente", function() {
								loadTable();
							});
						},
						error: function(e){
							console.log(e);
						}
					});
				}
			}
		}
	});
}

function changeStateElement(id) {
	var path = paths._delete;
	path = path.replace("idElemento", id);
	$.ajax({
		type: "POST",
		async: true,
		url: path,
		success: function(results) {
			bootbox.alert(entityName + " Editada/o correctamente", function() {
				loadTable();
			});
		},
	});
}

function documentOnReady() {
	if (isFirstLoadTable === true) {
		$(".btn-add").on("click", function () {
			addElement("new");
		});

		$(".btn-reload-table").on("click", function () {
			loadTable();
		});
	};

	$(".btn-details").on("click", function () {
		if ($(this).hasClass('disabled-entity')) {
			viewElementWithDisable($(this).data("id"), $(this).data('enabled'));
		} else {
			viewElement($(this).data("id"));
		}
	});

	if (hasDatatable === true) {
		$('#table-entities').dataTable( {
			"oLanguage": {
				"sUrl": sUrlDatatable,
				"iDisplayLength": 2
			}
		});
	};

	isFirstLoadTable = false;
}