<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $obras = $em->getRepository('AppBundle:Obra')->getObrasDashboard2();

        $allChecklists = $em->getRepository('AppBundle:Checklist')->findAll();
        $checklists = $em->getRepository('AppBundle:Checklist')->findBy([], ['fechaCompletado' => 'DESC'], 5);
        $checklistsCompletados = $em->getRepository('AppBundle:Checklist')->findBy(['finalizado' => true]);
        $checklistsPendientes = $em->getRepository('AppBundle:Checklist')->findBy(['finalizado' => null]);

        if (count($checklists) > 0) {
            $cumplimiento = count($checklistsCompletados) * 100 / count($allChecklists);
        } else {
            $cumplimiento = 0;
        }

        return $this->render('default/index.html.twig', [
            'obras' => $obras,
            'checklists' => $checklists,
            'checklistsCompletados' => $checklistsCompletados,
            'checklistsPendientes' => $checklistsPendientes,
            'cumplimiento' => round($cumplimiento)
        ]);
    }

    /**
     * @Route("reportes", name="reportes_index")
     */
    public function chartsIndex()
    {
        $em = $this->getDoctrine()->getManager();
        $obras = $em->getRepository('AppBundle:Obra')->findAll();

        return $this->render('AppBundle:Charts:index.html.twig',
            [
                'obras' => $obras
            ]
        );
    }

    /**
     * @param Request $request
     * @method ("POST")
     * @Route("reportes/comparacion", name="reportes_result_comparacion")
     */
    public function getChartsComparationResult(Request $request)
    {
        $obras = $request->request->get('obras');
        if(is_array($obras)) {
            $em = $this->getDoctrine();
            $rs = $em->getRepository('AppBundle:Obra')->getObrasDashboard($obras);
            echo $this->renderView(
                "AppBundle:Obra:columnaDetalleObra.html.twig",
                array(
                    "values" => $rs
                )
            );
        }else{
            echo "Seleccione Al Menos Una Obra";
        }

        die;
    }
}