<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class ObraController
 * @package AppBundle\Controller
 * @Route("/estado")
 * @author Guillermo Quinteros <gu.quinteros@gmail.com> 2015
 */
class EstadoController extends CrudController
{
    const ENTITY_NAME = "Estado";

    const ENTITY_NAMESPACE = "AppBundle\\Entity\\Estado";

    const TYPE_NAMESPACE = "AppBundle\\Form\\EstadoType";
}
