<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Checklist;
use AppBundle\Entity\Obra;
use AppBundle\Form\ObraType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ObraController
 * @package AppBundle\Controller
 * @Route("/obra")
 * @author Guillermo Quinteros <gu.quinteros@gmail.com> 2015
 */
class ObraController extends CrudController
{
    const ENTITY_NAME = "Obra";

    const ENTITY_NAMESPACE = "AppBundle\\Entity\\Obra";

    const TYPE_NAMESPACE = "AppBundle\\Form\\ObraType";

    /**
     * Displays a form to create a new entity.
     *
     * @Route("/new")
     * @param Request $request
     * @Method({"GET","POST"})
     * @Template()
     * @return array|Response
     */
    public function newAction(Request $request)
    {

        $entity = new Obra();
        $form =  $this->createForm(new ObraType(), $entity);

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em   = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                return new Response('success');
            }
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Lists all entities.
     *
     * @Route("/results")
     * @Method("GET")
     * @Template()
     */
    public function resultsAction()
    {
        $em       = $this->getDoctrine();
        $entities = $em->getRepository('AppBundle:Obra')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Get List of tareas.
     *
     * @Route("/add/Tareas", name="add_tareas")
     * @Method({"GET","POST"})
     * @Template()
     */
    public function addTareasAction()
    {
        $em       = $this->getDoctrine();
        $entities = $em->getRepository('AppBundle:Tarea')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Get save of tareas.
     * @param Request $request
     * @Route("/save/Tareas", name="save_tareas")
     * @Method("POST")
     * @return Response
     */
    public function saveTareasAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $forms = $request->request->get('form');
        $obra = $em->getRepository('AppBundle:Obra')->find($request->request->get('id'));
        foreach ($forms as $form) {
            if ($form['value'] != '')
            {
                $tarea = explode('-', $form['name']);
                $tareaId = $tarea[1];
                $tarea = $em->getRepository('AppBundle:Tarea')->find($tareaId);
                $checklist = $em->getRepository('AppBundle:Checklist')->findOneBy(
                    [
                        'obra' => $obra->getId(),
                        'tarea' => $tareaId
                    ]
                );

                if ($checklist)
                {
                    $checklist->setFechaComprometida(\DateTime::createFromFormat('d/m/Y', $form['value']));
                } else {
                    $checklist = new Checklist();
                    $checklist
                        ->setFechaComprometida(\DateTime::createFromFormat('d/m/Y', $form['value']))
                        ->setObra($obra)
                        ->setTarea($tarea);

            }

                $em->persist($checklist);
            }
        }

        $em->flush();

        return new Response('success');
    }

    /**
     * Get save of tareas.
     * @param Request $request
     * @Route("/completar/Tarea", name="complete_tareas")
     * @Method("POST")
     * @Template()
     * @return Response
     */
    public function completarTareaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $checklist = $em->getRepository('AppBundle:Checklist')->find($request->request->get('idChecklist'));
        $obra = $em->getRepository('AppBundle:Obra')->find($request->request->get('idObra'));

        return array(
            'tarea' => $checklist->getTarea(),
            'obra' => $obra
        );
    }

    /**
     * Get save of tareas.
     * @param Request $request
     * @param $id
     * @Route("/SaveCompletar/Tarea/{id}", name="save_complete_tareas")
     * @Method("POST")
     * @return Response
     */
    public function saveCompletarTareaAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $this->getRequest()->request->all();
        $file = $this->getRequest()->files->get('file');
        $observacion = $request->request->get('observacion');
        $checklist = $em->getRepository('AppBundle:Checklist')->find($id);
        if ($file) {
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            $pathDocument = $this->container
                    ->getParameter('kernel.root_dir') . '/../web/uploads/checklist';
            $file->move(
                $pathDocument,
                $fileName
            );
            $checklist->setFile($fileName);
        }

        if ($observacion) {
            $checklist->setObservacion($observacion);
        }

        $checklist->setFinalizado(true);
        $checklist->setFechaCompletado(new \DateTime());

        if ($checklist->getFechaComprometida() < new \DateTime()) {
            $checklist->setTareaAtrasada(true);
        }

        $em->persist($checklist);
        $em->flush();

        return new Response('success');
    }

    /**
     * Finds and displays a entity.
     *
     * @Route("/{id}")
     * @Method("GET")
     * @Template()
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine();
        $entity = $em->getRepository('AppBundle:Obra')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity.');
        }

        $checklists = $em->getRepository('AppBundle:Checklist')->getChecklistByObra($id);

        $checklistsCompletados = $em->getRepository('AppBundle:Checklist')->findBy(array(
            'obra' => $id,
            'finalizado' => true
        ));

        $checklistsPendientes = $em->getRepository('AppBundle:Checklist')->findBy(array(
            'obra' => $id,
            'finalizado' => null
        ));



        if (count($checklists) > 1) {
            $porcentaje = count($checklistsCompletados) * 100 / count($checklists);
        } else {
            $porcentaje = 0;
        }

        return array(
            'entity' => $entity,
            'checklists' => $checklists,
            'checklistCompletados' => $checklistsCompletados,
            'checklistPendientes' => $checklistsPendientes,
            'porcentaje' => round($porcentaje)
        );
    }

    /**
     * @Route("/verRendimiento/Obra/{id}", name="ver_rendimiento_obra")
     * @param $id
     * @return Response
     */
    public function showRedimientoObra($id)
    {
        $em = $this->getDoctrine()->getManager();
        $obraResult = $em->getRepository('AppBundle:Obra')->getRendimientoCumplimiento($id);
        $obra = $em->getRepository('AppBundle:Obra')->find($id);
        return $this->render('AppBundle:Charts:RendimientoCumplimientoResultBody.html.twig',
            [
                'obraResult' => $obraResult,
                'entity' => $obra
            ]
        );
    }

    /**
     * @Route("/verRendimiento/Obra/completo/{id}", name="ver_rendimiento_obra_completo")
     * @param $id
     * @return Response
     */
    public function showRedimientoObraCompleto($id)
    {
        $em = $this->getDoctrine()->getManager();
        $obraResult = $em->getRepository('AppBundle:Obra')->getRendimientoCumplimientoCompleto($id);
        $obra = $em->getRepository('AppBundle:Obra')->find($id);
        return $this->render('AppBundle:Charts:RendimientoCumplimientoResultBody.html.twig',
            [
                'obraResult' => $obraResult,
                'entity' => $obra
            ]
        );
    }

    /**
     * @Route("/verRendimiento/Obra/completoRendimiento/{id}", name="ver_rendimiento_obra_completo_rendimiento")
     * @param $id
     * @return Response
     */
    public function showRedimientoObraCompletoRendimiento($id)
    {
        $em = $this->getDoctrine()->getManager();
        $obraResult = $em->getRepository('AppBundle:Obra')->getRendimientoCumplimientoCompletoRendimiento($id);
        $obra = $em->getRepository('AppBundle:Obra')->find($id);
        return $this->render('AppBundle:Charts:RendimientoCumplimientoResultBody.html.twig',
            [
                'obraResult' => $obraResult,
                'entity' => $obra
            ]
        );
    }

    /**
     * @param Request $request
     * @Route("/editarFecha/checklist/{id}", name="editar_fecha_checklist")
     * @param $id
     * @return Response
     */
    public function editarFechaChecklist(Request $request, $id)
    {
        if ($request->getMethod() === 'POST') {
            $newDate = $request->request->get('date');
            $em = $this->getDoctrine()->getManager();
            $checklist = $em->getRepository('AppBundle:Checklist')->find($id);
            $checklist->setFechaComprometida(new \DateTime($newDate));
            $em->persist($checklist);
            $em->flush();
            return new Response('success');

        } else {
            $newDate = $request->query->get('date');
            return $this->render('AppBundle:Obra:EditarFechaChecklist.html.twig',
                [
                    'fecha' => $newDate
                ]
            );
        }
    }

    /**
     * @Route("/rendimiento/cumplimiento", name="rendimiento_cumplimiento")
     */
    public function getRendimientoCumplimientoIndex()
    {
        $em = $this->getDoctrine()->getManager();
        $obras = $em->getRepository('AppBundle:Obra')->findAll();

        return $this->render('AppBundle:Charts:RendimientoCumplimientoIndex.html.twig',
            [
                'obras' => $obras
            ]
        );
    }

    /**
     * @param Request $request
     * @method ("POST")
     * @Route("rendimiento/cumplimiento/result", name="rendimiento_cumplimiento_result")
     * @return Response
     */
    public function getRendimientoCumplimientoResult(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $obraResult = $em->getRepository('AppBundle:Obra')->getRendimientoCumplimiento($request->request->get('obra'));
        $obra = $em->getRepository('AppBundle:Obra')->find($request->request->get('obra'));
        return $this->render('AppBundle:Charts:RendimientoCumplimientoResult.html.twig',
            [
                'obraResult' => $obraResult,
                'entity' => $obra
            ]
        );

    }

    /**
     * @param Request $request
     * @method ("POST")
     * @Route("rendimiento/cumplimiento/completo/result", name="rendimiento_cumplimiento_completo_result")
     * @return Response
     */
    public function getRendimientoCumplimientoCompletoResult(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $obraResult = $em->getRepository('AppBundle:Obra')->getRendimientoCumplimientoCompleto($request->request->get('obra'));
        $obra = $em->getRepository('AppBundle:Obra')->find($request->request->get('obra'));
        return $this->render('AppBundle:Charts:RendimientoCumplimientoResult.html.twig',
            [
                'obraResult' => $obraResult,
                'entity' => $obra
            ]
        );

    }

    /**
     * @param Request $request
     * @method ("POST")
     * @Route("rendimiento/cumplimiento/completoRendimiento/result", name="rendimiento_cumplimiento_completo_result_rendimiento")
     * @return Response
     */
    public function getRendimientoCumplimientoCompletoResultRendimiento(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $obraResult = $em->getRepository('AppBundle:Obra')->getRendimientoCumplimientoCompletoRendimiento($request->request->get('obra'));
        $obra = $em->getRepository('AppBundle:Obra')->find($request->request->get('obra'));
        return $this->render('AppBundle:Charts:RendimientoCumplimientoResult.html.twig',
            [
                'obraResult' => $obraResult,
                'entity' => $obra
            ]
        );

    }

    /**
     * @Route("/acumulado/index", name="acumulado_index")
     */
    public function getAcumuladoIndex()
    {
        return $this
            ->render('AppBundle:Charts:AcumuladoIndex.html.twig');
    }

    /**
     * @param Request $request
     * @method ("POST")
     * @Route("acumulado/result", name="acumulado_result")
     * @return Response
     */
    public function getAcumuladoResult(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $acumulados = $em->getRepository('AppBundle:Obra')->getAcumulado();

        return $this->render('AppBundle:Charts:AcumuladoResult.html.twig',
            [
                'acumulado' => $acumulados
            ]
        );

    }

    /**
     * @param Request $id
     * @Route("/biblioteca/{id}", name="biblioteca_ver")
     * @param $id
     * @return Response
     */
    public function getChecklistWithFile($id)
    {
        $em = $this->getDoctrine()->getManager();
        $checklist = $em->getRepository('AppBundle:Checklist')->getChecklistWithFile($id);
        $obra = $em->getRepository('AppBundle:Obra')->find($id);

        return $this->render('AppBundle:Obra:biblioteca.html.twig',
            [
                'checklists' => $checklist,
                'obra' => $obra
            ]
        );
    }

    /**
     * @param Request $request
     * @Route("/editarAtraso/checklist/{id}", name="editar_atraso_checklist")
     * @param $id
     * @return Response
     */
    public function editarAtrasoChecklist(Request $request, $id)
    {
        if ($request->getMethod() === 'POST') {
            $em = $this->getDoctrine()->getManager();
            $checklist = $em->getRepository('AppBundle:Checklist')->find($id);
            $fechaComprometida = $checklist->getFechaComprometida();
            $checklist->setFechaCompletado($fechaComprometida);
            $checklist->setTareaAtrasada(FALSE);
            $em->persist($checklist);
            $em->flush();
            return new Response('success');
        } else {
            return $this->render('AppBundle:Obra:EditarAtrasoChecklist.html.twig');
        }
    }
}
