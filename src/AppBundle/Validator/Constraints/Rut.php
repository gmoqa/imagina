<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\MissingOptionsException;

/**
 * @Annotation
 */
class Rut extends Constraint
{
    public $message = 'Este valor no es un RUT valido';

    public function __construct($options = null)
    {
        parent::__construct($options);
    }
}