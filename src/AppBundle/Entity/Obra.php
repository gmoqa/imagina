<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Obra
 *
 * @author Guillermo Quinteros <gu.quinteros@gmail.com> 2015
 *
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="Obra")
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ObraRepository")
 */
class Obra
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     * @Assert\NotBlank(message="not_blank")
     * @ORM\Column(name="nombre", type="string", length=255, nullable=true)
     */
    private $nombre;

    /**
     * @var string
     * @Assert\NotBlank(message="not_blank")
     * @ORM\Column(name="direccion", type="string", length=255, nullable=true)
     */
    private $direccion;

    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="responsable", referencedColumnName="id")
     * })
     */
    private $responsable;

    /**
     * @var boolean
     *
     * @ORM\Column(name="finalizado", type="boolean", nullable=true)
     */
    private $finalizado;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Checklist", mappedBy="obra", cascade={"remove"})
     */
    protected $checklists;


    public function __construct()
    {
        $this->finalizado = 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param $nombre
     * @return $this
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param $direccion
     * @return $this
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * @return Usuario
     */
    public function getResponsable()
    {
        return $this->responsable;
    }

    /**
     * @param $responsable
     * @return $this
     */
    public function setResponsable($responsable)
    {
        $this->responsable = $responsable;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isFinalizado()
    {
        return $this->finalizado;
    }

    /**
     * @param $finalizado
     * @return $this
     */
    public function setFinalizado($finalizado)
    {
        $this->finalizado = $finalizado;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getChecklists()
    {
        return $this->checklists;
    }

    /**
     * @param $checklists
     * @return $this
     */
    public function setChecklists($checklists)
    {
        $this->checklists = $checklists;

        return $this;
    }
}
