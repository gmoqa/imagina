<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class ObraRepository
 *
 * @package AppBundle\EntityRepository
 *
 * @author  Guillermo Quinteros <gu.quinteros@gmail.com> 2016
 */
class UsuarioRepository extends EntityRepository
{
    public function getComparacionUsuarios($ids)
    {
        $em = $this->getEntityManager();

        $arrAux = [];
        $value=array();
        foreach ($ids as $id) {
            $dql = "SELECT
			usuario.id,
			usuario.nombres,
			usuario.apellidos,
			usuario.avatar
			FROM AppBundle:Checklist checklist
			JOIN checklist.obra obra
			JOIN obra.responsable usuario
			WHERE 1 = 1
			AND usuario.id = :usr
			";

            $query = $em->createQuery($dql);
            $query->setParameter('usr', $id);
            //Total de Tareas
            $results = $query->getResult();

            $dql = "SELECT
			usuario.id,
			usuario.nombres,
			usuario.apellidos
			FROM AppBundle:Checklist checklist
			JOIN checklist.obra obra
			JOIN obra.responsable usuario
			WHERE 1 = 1
			AND usuario.id = :usr
			AND checklist.finalizado IS NOT NULL
			";

            $query = $em->createQuery($dql);
            $query->setParameter('usr', $id);
            //Tareas Completadas
            $resultsCompletado = $query->getResult();

            $dql = "SELECT
			usuario.id,
			usuario.nombres,
			usuario.apellidos
			FROM AppBundle:Checklist checklist
			JOIN checklist.obra obra
			JOIN obra.responsable usuario
			WHERE 1 = 1
			AND usuario.id = :usr
			AND checklist.tareaAtrasada = TRUE
			";

            $query = $em->createQuery($dql);
            $query->setParameter('usr', $id);
            $resultsAtrasadas = $query->getResult();

            if (count($results) > 0) {
                $value["nombre"]=$results[0]["nombres"]." ".$results[0]["apellidos"];
                $porcentaje = (count($resultsCompletado)/count($results))*100;
                $value["avatar"] = $results[0]["avatar"];

                if (count($resultsCompletado)>0) {
                    $porcentaje = (count($resultsCompletado) / count($results)) * 100;
                } else {
                    $porcentaje = 0;
                }
                $value["porcentaje"] = round ($porcentaje);
                $value["atrasadas"] = count ($resultsAtrasadas);

                $value["total"] = count($results);
                $value["completados"] = count($resultsCompletado);
            } else {

                $value["nombre"] = "";
                $value["porcentaje"] = 0;
                $value["atrasadas"] = 0;
                $value["total"] = 0;
                $value["completados"] = 0;
            }
            $arrAux[]=$value;
        }
        return $arrAux;
    }

    public function getUsuariosValidos()
    {
        $em = $this->getEntityManager();
        $dql = "SELECT
			usuario.id,
			usuario.nombres,
			usuario.apellidos
			FROM AppBundle:Checklist checklist
			JOIN checklist.obra obra
			JOIN obra.responsable usuario
			WHERE 1 = 1
			GROUP BY usuario.id
			";

        $query = $em->createQuery($dql);
        return $query->getResult();
    }
}